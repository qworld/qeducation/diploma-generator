import os
import csv
import math
from jproperties import Properties
from PIL import Image, ImageDraw, ImageFont
import smtplib
import ssl
from email.message import EmailMessage
from email.policy import SMTP


class GenerateDiplomas:

    def __init__(self, file_with_config: str) -> None:
        self._attendees = []
        self._configs = Properties()
        with open(file_with_config, 'rb') as config_file:
            self._configs.load(config_file, "utf-8")

    def generate(self):

        # create folders
        if not os.path.isdir(self._configs.get('DIPLOMA_SAVE_PATH').data):
            os.mkdir(self._configs.get('DIPLOMA_SAVE_PATH').data)
        if not os.path.isdir(self._configs.get('DIPLOMA_PDF_SAVE_PATH').data):
            os.mkdir(self._configs.get('DIPLOMA_PDF_SAVE_PATH').data)


        # Create a secure SSL context for sending emails
        context = ssl.create_default_context()

        # Open connection, only once to send all emails
        with smtplib.SMTP_SSL(self._configs.get('EMAIL_SMTP').data,
                              self._configs.get('EMAIL_PORT').data,
                              context=context) as server:

            # Login to server if required
            if self._configs.get('EMAIL_SENDING').data.lower() == 'true':
                server.login(self._configs.get('EMAIL_ACCOUNT').data,
                             self._configs.get('EMAIL_PWD').data)

            print("Loading file ....")
            with open(self._configs.get('FILE_DATA_CSV').data,
                      encoding=self._configs.get('FILE_DATA_ENCODING_OPEN').data) as csv_file:
                csv_reader = csv.DictReader(csv_file)

                for attendee in csv_reader:
                    # if the attendee diploma should be generated
                    if self._deserves_diploma(attendee):
                        self._create_diploma(attendee)
                        self._send_diploma(attendee, server)
                    attendee['Processed'] = 'true'
                    self._attendees.append(attendee)

            print("Saving file ....")
            with open(self._configs.get('FILE_DATA_CSV').data[:-4] + '_processed.csv', mode='w',
                      newline='') as csv_file:
                csv_writer = csv.DictWriter(csv_file,
                                            fieldnames=[i for i in csv_reader.fieldnames] +
                                            ['Processed', 'Diploma generated', 'Diploma sent'])
                csv_writer.writeheader()
                for attendee in self._attendees:
                    csv_writer.writerow(attendee)

    def _deserves_diploma(self, attendee):

        # validate if attendee deserves diploma

        # ask for type of validation
        if self._configs.get('TYPE_REQUIREMENT_FOR_DIPLOMA').data.lower() == 'none':
            return True

        elif self._configs.get('TYPE_REQUIREMENT_FOR_DIPLOMA').data.lower() == 'simple':
            # type is simple by one field in csv file
            if attendee[self._configs.get('CSV_FIELD_GET_DIPLOMA').data].lower() == \
                    self._configs.get('STRING_FOR_GET_DIPLOMA').data.lower():
                return True

        return False

    def _create_diploma(self, attendee):

        if self._configs.get('DIPLOMA_GENERATION').data.lower() == 'false':
            return

        print("Creating diploma for:", attendee[self._configs.get('CSV_FIELD_NAME').data])

        img = Image.open(self._configs.get('FILE_DIPLOMA_TEMPLATE').data)
        width_img = img.width

        drw = ImageDraw.Draw(img)

        text_color = (int(self._configs.get('TEXT_COLOR_R').data),
                      int(self._configs.get('TEXT_COLOR_G').data),
                      int(self._configs.get('TEXT_COLOR_B').data))

        min_margin = float(self._configs.get('MIN_MARGIN_PERCENTAGE').data)
        left_margin = int(self._configs.get('LEFT_MARGIN_SIZE').data)

        width_img_with_margins = width_img - width_img * min_margin * 2 - left_margin

        # draw name
        self._draw_name(attendee, drw, width_img, text_color, left_margin, width_img_with_margins)

        # draw diploma number
        # self._draw_diploma_number(attendee, text_color, drw)
        self._draw_bottom_text(attendee, text_color, drw, width_img, left_margin)

        # save new diploma
        img.save(self._configs.get('DIPLOMA_SAVE_PATH').data +
                 self._configs.get('DIPLOMA_PREFIX_NAME').data +
                 attendee[self._configs.get('CSV_FIELD_ID').data] +
                 ".jpg")

        # save as pdf
        img.save(self._configs.get('DIPLOMA_PDF_SAVE_PATH').data +
                 self._configs.get('DIPLOMA_PREFIX_NAME').data +
                 attendee[self._configs.get('CSV_FIELD_ID').data] +
                 ".pdf")

        attendee['Diploma generated'] = 'true'

    def _draw_name(self, attendee, drw, width_img, text_color, left_margin, width_img_with_margins):

        font_for_name, name, plus_y = self._calculate_font_for_name(attendee, drw, width_img_with_margins)
        # font_for_name is just the font
        # name is the string of the name
        # plus_y determines how much to move the y of name if name is too big

        # a tuple of (x=0, y position + plus_y). don't need x for now.
        location = (0, int(self._configs.get('TEXT_NAME_Y_POSITION').data) + plus_y)

        # if multiple lines need this
        height_name = -1 * int(self._configs.get('REDUCE_Y_IF_TWO_LINES').data) if len(name) > 1 else 0

        for line in name:
            width_name, _ = font_for_name.getsize(line)
            if self._configs.get('TEXT_NAME_CENTERED').data.lower() == 'false':
                # if x position is specified in the config file
                text_name_location_x = int(self._configs.get('TEXT_NAME_X_POSITION').data)
            else:
                # otherwise, find the center of the writable area
                # see comment on left_margin in the config file.
                text_name_location_x = (width_img - left_margin - width_name) / 2 + left_margin

            # now have x and y location
            location = (text_name_location_x, location[1] + height_name)
            drw.text(location, line, fill=text_color, font=font_for_name, anchor='lm')
            _, height_name = font_for_name.getsize(line)
            height_name += int(self._configs.get('SPACE_BETWEEN_LINES').data)

    def _calculate_font_for_name(self, attendee, drw, width_img_with_margins):

        text = [attendee[self._configs.get('CSV_FIELD_NAME').data].title()]

        font_size = int(self._configs.get('DEFAULT_FONT_SIZE_NAME').data)

        # first use default font size
        font = ImageFont.truetype(self._configs.get('FONT_WRITE_DIPLOMA_NAME').data, font_size)

        width_txt, _ = drw.textsize(text[0], font=font)

        # we initially assume y is not moved at all
        plus_y = 0

        # but if name with normal font size is bigger than available area
        if width_txt >= width_img_with_margins:
            # re-calculate font size
            excess = width_txt / width_img_with_margins

            if excess-1 > float(self._configs.get('MAX_RESIZE_PERCENTAGE').data):
                # if the excess is greater than the maximum allowed, then it should be broken into two lines
                words = text[0].split(' ')
                first_line_words = math.ceil(len(words) / 2)
                text[0] = ' '.join(words[0:first_line_words])
                text.append(' '.join(words[first_line_words:]))
            else:
                # rescale according to excess
                font_size = int(font_size / excess)
                font = ImageFont.truetype(self._configs.get('FONT_WRITE_DIPLOMA_NAME').data, font_size)
                width_txt, _ = drw.textsize(text[0], font=font)
                # if the fontsize becomes smaller, then the center of the drawn text will 
                # automatically raise. So, we have to lower the center by increasing the y value.
                plus_y = int(2*(excess-1)*(int(self._configs.get('DEFAULT_FONT_SIZE_NAME').data)/10))

        return font, text, plus_y

    def _draw_bottom_text(self, attendee, text_color, drw, width_img, left_margin):

        font_dn = ImageFont.truetype(self._configs.get('FONT_WRITE_DIPLOMA_1').data,
                                     int(self._configs.get('FONT_SIZE_DIPLOMA_NUMBER').data))

        text = self._configs.get('TEXT_LECTURER').data

        diploma_number = self._configs.get('TEXT_DIPLOMA_NUMBER').data + \
            str(attendee[self._configs.get('CSV_FIELD_ID').data])

        text = text + "\n" + diploma_number

        width_text, _ = drw.textsize(text, font=font_dn)

        text_lecturers_location_x = (width_img - left_margin - width_text) / 2 + left_margin


        location_lecturers = (text_lecturers_location_x,
                              int(self._configs.get('TEXT_DN_Y_POSITION').data))

        drw.multiline_text(location_lecturers,
                           text,
                           fill=text_color,
                           font=font_dn,
                           anchor='lm',
                           align='center',
                           spacing=int(self._configs.get('TEXT_DN_SPACE_BETWEEN_LINES').data))

        diploma_number = self._configs.get('TEXT_DIPLOMA_NUMBER').data + \
            str(attendee[self._configs.get('CSV_FIELD_ID').data])

        # width_dn, _ = drw.textsize(diploma_number, font=font_dn)

        # text_dn_location_x = (width_img - left_margin - width_dn) / 2 + left_margin


        # location_dn = (text_dn_location_x,
        #                int(self._configs.get('TEXT_DN_Y_POSITION').data) +
        #                int(self._configs.get('TEXT_DN_SPACE_BETWEEN_LINES').data))


        # drw.text(location_dn, diploma_number, fill=text_color, font=font_dn, anchor='lm')

    def _send_diploma(self, attendee, server):

        if self._configs.get('EMAIL_SENDING').data.lower() == 'false':
            return

        print("Sending diploma to:", attendee[self._configs.get('CSV_FIELD_NAME').data],
              ", to email:", attendee[self._configs.get('CSV_FIELD_EMAIL').data])

        # Create message to be sent
        message = EmailMessage(policy=SMTP)
        message['Subject'] = self._configs.get('EMAIL_SUBJECT').data
        message['From'] = self._configs.get('EMAIL_ACCOUNT').data
        message['To'] = attendee['Email']



        part_plain = self._configs.get('EMAIL_CONTENT_TEXT').data.replace('{$name$}', attendee[self._configs.get('CSV_FIELD_NAME').data])
        part_html = self._configs.get('EMAIL_CONTENT_HTML').data.replace('{$name$}', attendee[self._configs.get('CSV_FIELD_NAME').data])

        message.set_content(part_plain)
        message.add_alternative(part_html, subtype='html')

        # attach img
        filename_img = self._configs.get('DIPLOMA_PREFIX_NAME').data + \
            str(attendee[self._configs.get('CSV_FIELD_ID').data]) + \
            ".jpg"
        filename_img_with_path = self._configs.get('DIPLOMA_SAVE_PATH').data + filename_img

        with open(filename_img_with_path, "rb") as attachment:
            message.add_attachment(attachment.read(), maintype='image',
                                   subtype='jpeg', filename=filename_img)

        # attach pdf
        filename_pdf = self._configs.get('DIPLOMA_PREFIX_NAME').data + \
            str(attendee[self._configs.get('CSV_FIELD_ID').data]) + \
            ".pdf"
        filename_pdf_with_path = self._configs.get('DIPLOMA_PDF_SAVE_PATH').data + filename_pdf

        with open(filename_pdf_with_path, "rb") as attachment:
            message.add_attachment(attachment.read(), maintype='application',
                                   subtype='pdf', filename=filename_pdf)


        # Send email
        server.send_message(message,
                            self._configs.get('EMAIL_ACCOUNT').data,
                            attendee[self._configs.get('CSV_FIELD_EMAIL').data])

        attendee['Diploma sent'] = 'true'
