# Diploma generator

Code to generate programmatically generate and send diplomas to workshop participants, mentors, interns etc. 


## Usage

### Installation
* First clone or download this repository.
* Install necessary packages 
```
$ pip install -r requirements.txt
```
#### Basic information
* All configurations are present in the 'bronze-config.properties' file. All variables mentioned in the instructions refer to modifying them in this file.


### Making sure diplomas generate correctly
* Place the diploma template image into the data directory. Set the `FILE_DIPLOMA_TEMPLATE` variable.
* Our first task is to determine where on the image to place the participant name, and the lecturer/diploma number information. To do this, run 
```
$ python find_text_location.py
```
Follow the instructions to determine the location of the participant name. Repeat to find the location of the lecturer/diploma number information. Place these in `TEXT_NAME_Y_POSITION` and `TEXT_DN_Y_POSITION` respectively. Edit other variables under the `Generate diplomas` heading in the configuration file.
* Now, let's generate some sample diplomas and see if they render correctly. First make sure `EMAIL_SENDING = False`. Then run
```
$ python main.py
```
The generated diplomas will be in 'diplomas-bronze/' directory by default. If they seem fine, you can move on to the next step. Otherwise, tweak the variables till you are satisfied.

### Making sure the mail template is correct
* Now, it is time to fix the email template. Edit in all information under the 'Information for sending emails' header in the configuration file. The `EMAIL_ACCOUNT` is usually one of the offical QWorld email addresses under your control. 
* Now open 'data/sample-csv.csv' and delete all lines except the header. Enter your own name and personal email address in the next line.
* Set `EMAIL_SENDING = True`. Then run
```
$ python main.py
```
Check the email you receive and make sure all the information is correct (don't forget the subject line), and both a jpg and pdf diploma is attached and open fine.
* Repeat the above steps till you are happy with the email template.

### Final recheck 
* Now place a csv file in the 'data' folder, in the same format as 'sample-csv.csv'. Edit `FILE_DATA_CSV` to this file.
* Set `EMAIL_SENDING = False`. Then run
```
$ python main.py
```
Make sure all diplomas generated for your participants are correct. For instance, make sure long names are rendered correctly, as are names with special characters.

### Time to send.
* Take a deep breath. Set `EMAIL_SENDING = True`. Then run
```
$ python main.py
```
Hopefully, all diplomas will be sent.
* It is likely that some emails will bounce. First, find the correct emails of the participants. Then, read the instructions under 'Requirements for diploma' in the configuration file. Modify the csv file appropriately and rerun 
```
$ python main.py
```
to send only the corrected diplomas.

## Support
For any sort of help, find Abdullah Khalid on discord.


## Authors and acknowledgment
Thanks to the following contributers:

* Claudia Zendejas-Morales





