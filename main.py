from generate_diplomas import GenerateDiplomas

# Generate diplomas (and send them) for QBronze part of Summer School
gd_for_bronze = GenerateDiplomas('bronze-config.properties')
gd_for_bronze.generate()

# Generate diplomas (and send them) for QSilver part of Summer School
#gd_for_silver = GenerateDiplomas('silver-config.properties')
#gd_for_silver.generate()