from jproperties import Properties
from PIL import Image, ImageDraw, ImageTk
import tkinter as tk
import copy

# fix for images that are too large

file_with_config = 'bronze-config.properties'
configs = Properties()
with open(file_with_config, 'rb') as config_file:
    configs.load(config_file, "utf-8")

# increase this if the lines are too narrow
linewidth = 1

maxsize = (1000, 1000)


print("The name of the participant should be centered in some region of the diploma image.")
print("We will determine the top and bottom of this region.")
print("First the image is shown with horizontal lines every 100 pixels.")
print("This should give you a rough estimate of the boundaries of the region.")
print("Close the image once you are done.")


root = tk.Tk()
img = Image.open(configs.get('FILE_DIPLOMA_TEMPLATE').data)

# get original image dimensions
org_width_img = img.width
org_height_img = img.height


# create smaller image and get dimensions
img_thumb = copy.deepcopy(img)
img_thumb.thumbnail(maxsize)
width_img = img_thumb.width
height_img = img_thumb.height

img_thumb_backup = copy.deepcopy(img_thumb)

# draw image with lines
drw = ImageDraw.Draw(img_thumb)
for y in range(0, height_img, 100):
    drw.line((0, y, width_img, y), fill=(255, 0, 0), width=linewidth)
    drw.text((10, y-20), str(y), fill=(255, 0, 0))
tkimage = ImageTk.PhotoImage(img_thumb)
tk.Label(root, image=tkimage).pack()
root.mainloop()

print("")
print("Now we find the y-coordinate of the top of the region.")

while True:

    userystr = input("Enter a guess for the y-coordinate: ")
    usery = int(userystr)
    if usery == -1:
        break


    img_thumb = copy.deepcopy(img_thumb_backup)
    drw = ImageDraw.Draw(img_thumb)
    drw.line((0, usery, width_img, usery), fill=(255, 0, 0), width=linewidth)

    root = tk.Tk()
    tkimage = ImageTk.PhotoImage(img_thumb)
    tk.Label(root, image=tkimage).pack()
    root.mainloop()
    ytop = usery
    print("If you are happy with your guess enter -1 in the following box, or enter a new guess.")

ytop = ytop * org_height_img/height_img
print("top y = ", ytop)

print("")
print("Now we find the y-coordinate of the bottom of the region.")
print("We show the image again with the horizontal rules.")
print("Close the image once you are done.")





img_thumb = copy.deepcopy(img_thumb_backup)
drw = ImageDraw.Draw(img_thumb)
for y in range(0, height_img, 100):
    drw.line((0, y, width_img, y), fill=(255, 0, 0), width=linewidth)
    drw.text((10, y-20), str(y), fill=(255, 0, 0))

root = tk.Tk()
tkimage = ImageTk.PhotoImage(img_thumb)
tk.Label(root, image=tkimage).pack()
root.mainloop()


while True:

    userystr = input("Enter a guess for the y-coordinate: ")
    usery = int(userystr)
    if usery == -1:
        break



    img_thumb = copy.deepcopy(img_thumb_backup)
    drw = ImageDraw.Draw(img_thumb)
    drw.line((0, usery, width_img, usery), fill=(255, 0, 0), width=linewidth)

    root = tk.Tk()
    tkimage = ImageTk.PhotoImage(img_thumb)
    tk.Label(root, image=tkimage).pack()
    root.mainloop()
    ybot = usery
    print("If you are happy with your guess enter -1 in the following box, or enter a new guess.")

ybot = ybot * org_height_img/height_img
print("bottom y = ", ybot)

recommendationy = int((ytop+ybot)/2)

print("In the diplomas, your text should ideally be located at, ", recommendationy)
